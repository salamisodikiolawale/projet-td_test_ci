# from mock import Mock
from mock.mock import Mock
from CartePizzeria import CartePizzeria

def test_is_empty() :
    carte = CartePizzeria()
    res1 = True
    assert carte.is_empty() == res1

def test_is_not_empty():
    carte = CartePizzeria()
    pizza = Mock()
    carte.pizzas = [pizza]
    return not carte.is_empty()

def test_add_pizzare():
    carte = CartePizzeria()
    pizza = Mock()
    list_expected = [pizza]
    carte.add_pizza(pizza)
    assert carte.pizzas == list_expected

def test_nb_pizzas():
    carte = CartePizzeria()
    pizza = Mock()
    carte.pizzas = [pizza]
    assert len(carte.pizzas) == 1
