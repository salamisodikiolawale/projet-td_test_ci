class CartePizzeria:

    def __init__(self) -> None:
        self.pizzas = list()

    def is_empty(self) :
        return len(self.pizzas) == 0

    def nb_pizzas(self):
        return len(self.pizzas)

    def add_pizza(self, pizza):
        self.pizzas.append(pizza)

    def remove_pizza(self, name):
        is_pizza_in = False
        for i in self.pizzas:
            if self.pizzas[i].name == name:
                pizza_is_in = True
                del self.pizzas[i]
            
        if not is_pizza_in:
             return Exception("not exist")